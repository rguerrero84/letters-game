import React from 'react';
import Head from 'next/head';
import uuidv4 from 'uuid/v4';
import NotificationSystem from 'react-notification-system';
import LetterWrapper from '../components/letter-wrapper';
import { getBoard, validateAnswer, getScore } from '../services/requests';
import '../styles/HomeContainer.css';

class HomeContainer extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {

            rows: null,
            word: [],
            notification: '',
            resetButtonDisabled: true,
            score: 0,
            functions: {

                setLetter: this.setLetter,
                removePosition: this.removePosition,
            }
        };
    }

    chunk = (arr, size) => {

        return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) => arr.slice(i * size, i * size + size));
    }

    setLetter = async(letter, position) => {
        const word = this.state.word.slice(0);
        word.push({

            letter,
            position,
        });

        this.setState({

            word,
            resetButtonDisabled: false,
        });

        const user = JSON.parse(window.localStorage.getItem('user'));
        const validationModel = {

            user,
            word,
        };

        const response = await validateAnswer(validationModel);
        const content = await response.json();
        const status = content.status;

        if (status === 'valid') {

            getScore(user.id).then(async(response) => {
                const content = await response.json();

                this.setState({

                    score: content.score,
                });
            });

            window.setTimeout(() => this.resetWord(), 2000);
        }

        this.setState({

            notification: status,
        });
    }

    removePosition = (position) => {
        let word = this.state.word.slice(0);
        word = word.filter((item) => item.position !== position);

        this.setState({

            word,
            notification: (word.length === 0 ? '': this.state.notification),
            resetButtonDisabled: (word.length === 0),
        });
    }

    resetWord = () => {

        this.setState({

            word: [],
            notification: '',
            resetButtonDisabled: true,
        });
    }

    componentDidMount = async() => {

        try{

            if (window.localStorage) {

                let user = await window.localStorage.getItem('user');
                if (user === null) {
    
                    window.localStorage.setItem('user', JSON.stringify({ id: uuidv4() }));
                }
                
                user = JSON.parse(user);
                getScore(user.id).then(async(response) => {
                    const content = await response.json();
    
                    this.setState({
    
                        score: content.score,
                    });
                });
            }

            const response = await getBoard();
            if (!response.ok) {

                const error = await response.json();
                this.NotificationSystem.addNotification({
                    title: 'Advertencia',
                    message: error,
                    level: 'error',
                    autoDismiss: 5,
                });
                return;
            }

            const content = await response.json();
            this.setState({ rows: this.chunk(content.board, 4) });
        } catch(error) {

            this.NotificationSystem.addNotification({
                title: 'Advertencia',
                message: 'No se ha podido conectar al servidor.',
                level: 'error',
                autoDismiss: 5,
            });
        }
    }

    render() {
        const rows = this.state.rows;

        return (
            <div className="home-container-wrapper">
                <NotificationSystem ref={(component) => (this.NotificationSystem = component)} />
                <Head>
                    <meta httpEquiv='Content-type' content='text/html; charset=utf-8' />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                    <meta name="description" content='This is an awesome letters game!!!' />
                    <link rel='shortcut icon' type='image/png' href='/static/images/favicon.png'/>
                    <title>Letters Game!</title>
                </Head>
                <div className="title">
                    Letters Game! | Score: {this.state.score}
                </div>
                {rows !== null &&
                    <div className="wrapper">
                        <div className="board-wrapper">
                            {
                                rows.map((row, indexRow) => {
                                    row = row.map((letter, indexLetter) => (<LetterWrapper notification={this.state.notification} position={`${indexRow}/${indexLetter}`} word={this.state.word} functions={this.state.functions} letter={letter} key={indexLetter} />));

                                    return(
                                        <div className="row" key={indexRow}>
                                            {row}
                                        </div>
                                    );
                                })
                            }
                        </div>
                        <div className="actions">
                            <div className="clear-buttom-wrapper">
                                <button style={this.state.resetButtonDisabled ? { opacity: 0.5 } : {}} onClick={() => this.resetWord()} disabled={this.state.resetButtonDisabled}>
                                    <span>clear word</span>
                                    <span>X</span>
                                </button>
                            </div>
                            <div className="input-wrapper">
                                <input readOnly type="text" value={this.state.word.map((item) => item.letter).join('')} className={this.state.notification === 'valid' ? 'noselect valid-rule-text' : 'noselect invalid-rule-text'} />
                                <div className={this.state.notification === 'valid' ? 'notifier valid-rule-text' : 'notifier invalid-rule-text'}>{this.state.notification}</div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}
  
export default HomeContainer;