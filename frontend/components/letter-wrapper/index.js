/**
*
* LetterWrapper
*
*/

import React from 'react';
import PropTypes from 'prop-types';

function LetterWrapper(props) {
  const taken = props.word.some((item) => item.position === props.position);
  const className = !taken ? 'letter' : (props.notification === 'valid' ? 'letter valid-rule' : 'letter invalid-rule');

  return (
    <div className={className}>
        <button onClick={() => taken ? props.functions.removePosition(props.position) : props.functions.setLetter(props.letter, props.position)}>
          <span>{props.letter}</span>
        </button>
    </div>
  );
}

LetterWrapper.propTypes = {
    letter: PropTypes.string.isRequired,
    word: PropTypes.array.isRequired,
    position: PropTypes.string.isRequired,
    notification: PropTypes.string.isRequired,
    functions: PropTypes.object.isRequired,
};

export default LetterWrapper;