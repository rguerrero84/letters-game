import { fetchData, sendPOST } from './apiManager';
import { BACKEND_SERVER_HOST } from '../config/constants';
const CONFIG = `${BACKEND_SERVER_HOST}/api/`;

export function getBoard() {
    const url = `${CONFIG}board`;
    return fetchData(url, {});
}

export function validateAnswer(model) {
    const url = `${CONFIG}answers/validate`;
    return sendPOST(url, model);
}

export function getScore(userId) {
    const url = `${CONFIG}answers/myscore?userId=${userId}`;
    return fetchData(url, {});
}