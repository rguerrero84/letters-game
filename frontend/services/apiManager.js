export async function callApi(endpoint, options = {}) {
    const currentOptions = options;
    currentOptions.cache = 'default';

    return fetch(endpoint, currentOptions).then((response) => {

        return Promise.resolve(response);
    });
}

export function sendPOST(endpoint, body = {}, headers = {}) {
    return callApi(endpoint, {
      method: 'POST',
      headers: Object.assign({}, { 'Content-Type': 'application/json' }, headers),
      body: JSON.stringify(body),
    });
}
  
export function sendDELETE(endpoint, headers = {}) {
    return callApi(endpoint, {
      method: 'DELETE',
      headers: Object.assign({}, { 'Content-Type': 'application/json' }, headers),
    });
}
  
export function sendDELETEWithContent(endpoint, body = {}, headers = {}) {
    return callApi(endpoint, {
      method: 'DELETE',
      headers: Object.assign({}, { 'Content-Type': 'application/json' }, headers),
      body: JSON.stringify(body),
    });
}
  
export function sendPUT(endpoint, body = {}, headers = {}) {
    return callApi(endpoint, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: Object.assign({}, { 'Content-Type': 'application/json' }, headers),
    });
}
  
export function sendPostFormData(endpoint, body, headers = {}) {
    return callApi(endpoint, {
      method: 'POST',
      headers: Object.assign({}, headers),
      body,
    });
}
  
export function fetchData(endpoint, headers = {}) {
    return callApi(endpoint, {
      method: 'GET',
      headers: Object.assign({}, { 'Content-Type': 'application/json' }, headers),
    });
}