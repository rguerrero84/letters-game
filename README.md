# README #

Letters Game

#SETUP - FRONTEND
#Nextjs SSR
cd frontend/
npm install
npm run build
npm run start

#SETUP - BACKEND
#EXPRESS / lowdb (embedded database)
cd backend/
npm install
npm run start

#PART 3
1) I would suggest not to set mongodb credentials on the general code. (use dotenv to load credentials from process enviroment)
2) Create a mongo pool to avoid open/close connections on demand (using the option poolSize in the mongo client and assign the db var to a global one for eventual uses)
3) Use express for a solid nodejs-base app.
4) create a config file for express (root/config/config.js)
5) Create a well-known project structure for better coding and import js files on demand.
6) sanitize and validades income data in each route (use express-validator)
7) convert saveUser function to an es6 async function to avoid delay on mongo transaction and UI blocking. In general not use sync operations.
8) Code API better, using http methods like GET,PUT,POST,DELETE.
9) Use cors in config to avoid undesired domain requests and express-rate-limit to limit request per endpoint.
10) Have the seeding logic apart.
11) Move all to ES6 using only imports, arrow functions, etc.
12) use const intead of let when needed.
13) use body-parser to convert incoming data to json by default. And use res.json() for json responses instead of JSON.stringify
