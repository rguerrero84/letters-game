import path from 'path';
import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';

const adapter = new FileSync(path.join(__dirname, '../private/db/db.json'));
const db = low(adapter);

const exists = db.has('users').value();
if (!exists) { db.defaults({ users: [] }).write(); }

export default db;