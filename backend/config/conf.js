import helmet from 'helmet';
import compression from 'compression';
import bodyParser from 'body-parser';
import rateLimit from 'express-rate-limit';
import express from 'express';
const router = express.Router();
const app = express();

/* router */
app.enable("trust proxy");
app.use('/api', router);

/* Static Files */
app.use('/static', express.static('public'));

/* Helmet */
router.use(helmet());

/* compression */
router.use(compression());

/* Body Parser */
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

/* Limit */
const apiLimiter = rateLimit({

    windowMs: 1 * 60 * 1000,
    max: 20,
    statusCode: 403
});

router.use("/api/", apiLimiter);

/* Cors */
router.use(function(req, res, next) {

    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(8080);

export default router;