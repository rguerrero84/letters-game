import fs from 'fs';
import path from 'path';
import db from '../config/db';

/* Score Singleton */
global.scoreSingleton = {};

const api = (router) => {

    /* Load board.json async and shuffle it */
    let shuffleData = null;
    fs.readFile(path.join(__dirname, '../private/files/board.json'), 'utf8', (err, contents) => {
        
        shuffleData = JSON.parse(contents);
        shuffleData.board = shuffleData.board
                            .map((letter) => [Math.random(), letter])
                            .sort((a, b) => a[0] - b[0])
                            .map((letter) => letter[1]);
    });

    /* Load possible answers */
    let answers = null;
    fs.readFile(path.join(__dirname, '../public/files/dictionary.json'), 'utf8', (err, contents) => {

        answers = JSON.parse(contents).words;
    });

    router.get('/board', function (req, res) {

        res.json(shuffleData);
    });

    router.post('/answers/validate', function (req, res) {

        const user = req.body.user;
        const word = req.body.word;
        const generatedWord = word.map((item) => item.letter).join('');

        const isSuccessful = answers.includes(generatedWord.toLowerCase());

        const userObject = db.get('users').find({ id:  user.id }).value();        
        if (isSuccessful) {

            const data = scoreSingleton[user.id];
            if (data === undefined) {

                scoreSingleton[user.id] = { score: 1 };
            } else {

                data.score++;
            }

            if (userObject === undefined) {

                db.get('users').push({ id: user.id, entries: [{ score: scoreSingleton[user.id].score, created: new Date() }] }).write();
            } else {

                const entries = userObject.entries;
                entries.push({ score: scoreSingleton[user.id].score, created: new Date() });

                db.get('users').find({ id:  user.id }).assign({ entries }).write();
            }
        }

        res.json(isSuccessful ? { status: 'valid', alreadyExists: userObject !== undefined } : { status: 'invalid', alreadyExists: userObject !== undefined });
    });

    router.get('/answers/myscore', function (req, res) {

        const userId = req.query.userId;
        const data = scoreSingleton[userId];

        res.json(data !== undefined ? { score: data.score } : { score: 0 });
    });
};

export default api;